(require 'autothemer)

(autothemer-deftheme
 fez-purple "A purple theme created by FeZoli"

 ((((class color min-colors #xFFFFFF)))
 
 ;; Define our color palette
 (fez-purple-fg           "#302233")
 (fez-purple-bg           "#fef0ff")
 (fez-purple-active       "#746cc0")
 (fez-purple-comment      "red4")
 (fez-purple-cursor       "DarkOrange2")
 (fez-purple-emph1        "RoyalBlue4")
 (fez-purple-inactive     "#c3b4ec")
 (fez-purple-string       "ForestGreen")
 (fez-purple-region       "DarkOrange")
 (fez-purple-type         "DarkOrchid3")
 (fez-purple-var          "blue")

 )

 ;; Customize faces
 ((default                      (:foreground fez-purple-fg     :background fez-purple-bg))
  (company-preview              (:background fez-purple-active))
  (company-tooltip              (:background fez-purple-inactive))
  (company-tooltip-selection    (:foreground fez-purple-bg :background fez-purple-active :weight 'bold))
  (cursor                       (:background fez-purple-cursor))
  (fringe                       (:background fez-purple-bg))
  (header-line                  (:background fez-purple-fg))
  (highlight                    (:background fez-purple-region))
  (mode-line                    (:foreground fez-purple-bg     :background fez-purple-active))
  (mode-line-inactive           (:foreground fez-purple-fg     :background fez-purple-inactive))
  (region                       (:background fez-purple-region))
  (eglot-highlight-symbol-face  (:background fez-purple-region))
  (font-lock-keyword-face       (:foreground fez-purple-emph1))
  (font-lock-string-face        (:foreground fez-purple-string))
  (font-lock-comment-face       (:foreground fez-purple-comment))
  (font-lock-preprocessor-face  (:foreground fez-purple-comment))
  (font-lock-type-face          (:foreground fez-purple-type))
  (font-lock-variable-name-face (:foreground fez-purple-var))
  (font-lock-constant-face      (:foreground fez-purple-type))
  (lsp-headerline-breadcrumb-path-face (:foreground fez-purple-bg))
  (lsp-headerline-breadcrumb-symbols-face (:foreground fez-purple-bg :weight 'bold))
  (window-divider              (:foreground fez-purple-inactive))
  (window-divider-first-pixel  (:foreground fez-purple-inactive))
  (window-divider-last-pixel   (:foreground fez-purple-inactive))
  )
 )

(provide-theme 'fez-purple)
