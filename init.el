;; use-package ;;
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-and-compile
  (setq use-package-always-ensure t
        use-package-expand-minimally t
	command-log-mode t))

;; manual plugin storage
(setq plugin-path "~/.emacs.d/plugins")
(add-to-list 'load-path plugin-path)

;; apply local settings if any
(setq file-path (concat plugin-path "/local.el"))
(if (file-exists-p file-path)
    (load file-path))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(current-language-environment "UTF-8")
 '(custom-enabled-themes '(fez-purple))
 '(custom-safe-themes
   '("04d8242b2fcdb78c095d5edd7e559924b2219ecfab49c0929e6b6a2e98836b3a" "c045b33ee4b0bda6a2fa20ce4074bd04b14fad8f9f504fb7df3aafccb0d4efae" "df4d38bc347eb206d7c7e5cd8d2a2c16dd22f7ba936a23672b7e06f046c34fcc" "f4080579e23c9134ce08c9c608477b58020e100d339a402ae91e1e493cb6c717" "2ca9014494e44356494afd6d0695d745e83d88afc9ccc9feb04632bcb58d66b3" default))
 '(delete-by-moving-to-trash t)
 '(electric-pair-mode nil)
 '(electric-pair-text-pairs '((34 . 34) (40 . 41) (60 . 62) (123 . 125)))
 '(grep-command "grep -n -I -r ")
 '(inhibit-startup-screen t)
 '(package-selected-packages
   '(json-rpc editorconfig fancy-compilation multi-web-mode git-gutter-fringe+ company autothemer ac-js2 js2-mode rainbow-mode line-reminder goto-last-change hl-block-mode highlight-blocks realgud-lldb vertico cmake-mode yaml-mode web-mode-edit-element rust-mode php-mode lua-mode fish-mode dockerfile-mode))
 '(warning-suppress-log-types '((comp) (comp) (comp)))
 '(warning-suppress-types '((comp) (comp) (comp) (comp))))
;; cmake warnings

;; be sure all packages are installed
(package-install-selected-packages)

(global-tab-line-mode 0)
(scroll-bar-mode 0)
(tool-bar-mode 0)
(menu-bar-mode 0)
(global-display-line-numbers-mode 1)
;(set-fringe-mode 0) ; left and right empty space
(show-paren-mode t)
(column-number-mode 1)
(blink-cursor-mode 1)
(cua-mode 1)
(fancy-compilation-mode 1)

(setq fancy-compilation-override-colors 1)
(setq compilation-skip-threshold: 2) ;; skip warnings and info
(setq auto-save-default nil)
(setq visible-bell t)
(setq org-support-shift-select t)

(set-face-attribute 'default nil
		    :family "SourceCodePro"
		    :height 120
		    :width 'normal)
(prefer-coding-system 'utf-8)

(defun enlarge-window-h10 ()
  (interactive)
  (enlarge-window-horizontally 10))
(defun shrink-window-h10 ()
  (interactive)
  (shrink-window-horizontally 10))
(defun open-user-init-file ()
  (interactive)
  (find-file user-init-file))

;;(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(global-set-key (kbd "<f1>")     'delete-other-windows-internal)
(global-set-key (kbd "S-<f1>")   'gdb-restore-windows)
(global-set-key (kbd "C-S-l")    'enlarge-window-h10)
(global-set-key (kbd "C-S-h")    'shrink-window-h10)
(global-set-key (kbd "<f2>")     'xref-find-definitions)
(global-set-key (kbd "<f3>")     'isearch-repeat-forward)
(global-set-key (kbd "S-<f3>")   'isearch-backward)
(global-set-key (kbd "<f4>")     'goto-line)
(global-set-key (kbd "<f5>")     'project-compile)
(global-set-key (kbd "<f6>")     'gdb)
(global-set-key (kbd "S-<f6>")   'gdb-attach)
(global-set-key (kbd "<f7>")     'gud-step)
(global-set-key (kbd "S-<f7>")   'gud-finish)
(global-set-key (kbd "<f8>")     'gud-next)
(global-set-key (kbd "<f9>")     'imenu)
(global-set-key (kbd "<f10>")    'xref-find-references)
(global-set-key (kbd "<f11>")    'eldoc)
(global-set-key (kbd "<f12>")    'open-user-init-file)

(global-set-key (kbd "C-<tab>")    'completion-at-point)
(global-set-key (kbd "\C-z")     'undo)
(global-set-key (kbd "<insert>") 'other-window)
(global-set-key (kbd "S-<insert>") 'window-swap-states)
(global-set-key (kbd "C-x v =")  'vc-ediff)
(global-set-key (kbd "C-f")      'isearch-forward)
(global-set-key (kbd "C-S-s")    'save-some-buffers)
(global-set-key (kbd "C-o")      'project-find-file)
(global-set-key (kbd "<M-O>")    'project-find-regexp)

(global-set-key (kbd "\C-c y k r") 'yank-from-kill-ring)
(global-set-key (kbd "\C-c p f f") 'project-find-file)
(global-set-key (kbd "\C-c p f r") 'project-find-regexp)
(define-key isearch-mode-map "\C-f" 'isearch-repeat-forward)

(defun add-mode-line-dirtrack ()
  "When editing a file, show the last 2 directories of the current path in the mode line."
  (add-to-list 'mode-line-buffer-identification 
	       '(:eval (substring default-directory 
				  (+ 1 (string-match "/[^/]+/[^/]+/$" default-directory)) nil))))  
(add-hook 'find-file-hook 'add-mode-line-dirtrack)
(put 'dired-find-alternate-file 'disabled nil)

;; build
(setq compilation-auto-jump-to-first-error nil)
(setq compilation-ask-about-save nil)
(setq compilation-scroll-output t)
(setq compilation-always-kill t)
(setq compilation-window-height 15)
(setq compile-command "cmake --build build/ -j 1")

;; debug
(setq gdb-many-windows t)
(setq gdb-show-main t)
(setq gdb-restore-window-configuration-after-quit t)
(setq gdb-default-window-configuration-file "gdb.layout")
(setq gdb-debuginfod-enable-setting nil)

;; cmake
(use-package cmake-mode
   :mode
   ("CMakeLists.txt'" . cmake-mode))

;; eglot
(with-eval-after-load 'eglot
  (add-to-list 'eglot-server-programs
               '((c-mode c++-mode)
                 . ("clangd"
                    "--background-index"
                    "--completion-style=detailed"
                    "--pch-storage=memory"
                    "--header-insertion=never"
                    "--header-insertion-decorators=0"))))
(setq eglot-ignored-server-capabilities '(:documentOnTypeFormattingProvider :inlayHintProvider))

;; C/C++ development IDE
(add-hook 'c++-mode-hook
	  '(lambda ()
	     (condition-case nil
		 (progn
		   ;; debugging
		   ;; language server
		   (require 'eglot)
		   (eglot-ensure)
		   (require 'git-gutter+)
		   (git-gutter+-mode)
		   (require 'company)
		   (company-mode)
		   (c-set-style "ellemtel")
		   (which-function-mode)
		   ;;(setq c-default-style '((LSY . "LSY")))
		   ;;(c-add-style "LSY" lsy-c-style)
		   (setq-local c-basic-offset 3)
		   (message "c++ IDE mode activated")
		   )
	       (error (message "Install clangd to have a c++ IDE!"))
	       )
	     )
	  )

(defconst lsy-c-style
  '((c-offsets-alist            . ((inexpr-class . +)
				   (inexpr-statement . +)
				   (lambda-intro-cont . +)
				   (inlambda . 0)
				   (template-args-cont c-lineup-template-args +)
				   (incomposition . +)
				   (inmodule . +)
				   (innamespace . +)
				   (inextern-lang . +)
				   (composition-close . 0)
				   (module-close . 0)
				   (namespace-close . 0)
				   (extern-lang-close . 0)
				   (composition-open . 0)
				   (module-open . 0)
				   (namespace-open . 0)
				   (extern-lang-open . 0)
				   (objc-method-call-cont c-lineup-ObjC-method-call-colons c-lineup-ObjC-method-call +)
				   (objc-method-args-cont . c-lineup-ObjC-method-args)
				   (objc-method-intro . [0])
				   (friend . 0)
				   (cpp-define-intro c-lineup-cpp-define +)
				   (cpp-macro-cont . +)
				   (cpp-macro . [0])
				   (inclass . +)
				   (stream-op . c-lineup-streamop)
				   (arglist-cont-nonempty c-lineup-gcc-asm-reg c-lineup-arglist)
				   (arglist-cont c-lineup-gcc-asm-reg 0)
				   (comment-intro c-lineup-knr-region-comment c-lineup-comment)
				   (if-clause . 0)
				   (catch-clause . 0)
				   (else-clause . 0)
				   (do-while-closure . 0)
				   (access-label . -)
				   (case-label . 0)
				   (substatement . 0)
				   (statement-case-intro . +)
				   (statement . 0)
				   (brace-entry-open . 0)
				   (brace-list-entry . 0)
				   (brace-list-close . 0)
				   (block-close . 0)
				   (block-open . 0)
				   (inher-cont . c-lineup-multi-inher)
				   (inher-intro . +)
				   (member-init-cont . c-lineup-multi-inher)
				   (member-init-intro . +)
				   (annotation-var-cont . +)
				   (annotation-top-cont . 0)
				   (topmost-intro . 0)
				   (knr-argdecl . 0)
				   (func-decl-cont . +)
				   (inline-close . 0)
				   (class-close . 0)
				   (class-open . 0)
				   (defun-block-intro . +)
				   (defun-close . 0)
				   (defun-open . 0)
				   (c . c-lineup-C-comments)
				   (string . c-lineup-dont-change)
				   (topmost-intro-cont first c-lineup-topmost-intro-cont c-lineup-gnu-DEFUN-intro-cont)
				   (brace-list-intro first c-lineup-2nd-brace-entry-in-arglist c-lineup-class-decl-init-+ +)
				   (brace-list-open . 0)
				   (inline-open . 0)
				   (arglist-close . c-lineup-arglist)
				   (arglist-intro . c-lineup-arglist-intro-after-paren)
				   (statement-cont . +)
				   (statement-case-open . +)
				   (label . +)
				   (substatement-label . 0)
				   (substatement-open . 0)
				   (knr-argdecl-intro . 5)
				   (statement-block-intro . +)))
    (c-echo-syntactic-information-p . t))
  "LSY C/C++ Programming Style")
;; C/C++ development IDE end ;;

;; Fire up rust
(add-to-list 'auto-mode-alist '("\\.rs$'" . rust-mode))
(defun setup-rust-mode ()
  (setq indent-tabs-mode nil)
  (prettify-symbols-mode 0)
  (helm-mode 1)
  (projectile-mode 1)
  (setq-local rust-enable-format-on-save 1)
  (local-set-key [f5] 'rust-compile)
  (local-set-key [f6] 'rust-run)
  (local-set-key (kbd "C-M-o") 'projectile-find-file))
(add-hook 'rust-mode-hook
	  (lambda ()
	     (condition-case nil
		 (progn
		   (require 'rust-mode)
		   (require 'helm)
		   (setup-rust-mode)
		   (message "Rust IDE mode activated")
		   )
	       (error (message "Install rust-analyze, and packages helm, projectile to have a Rust IDE!"))
	       )
	     )
 )

;; php
;;

;; python
(add-hook 'python-mode-hook
	  (lambda ()
	    (eglot-ensure)))

;; javascript
(use-package js2-mode
  :mode
  ("\\.js\\'" . js2-mode))

;; Fire up template mode
(use-package web-mode-edit-element)
(use-package web-mode
  :mode
  ("\\.tpl.php\\'" . web-mode)
  :config
  (setq web-mode-markup-indent-offset 3)
  (setq web-mode-css-indent-offset 3)
  (setq web-mode-code-indent-offset 3)
  :hook
  (web-mode-hook . web-mode-edit-element-minor-mode))

(use-package vertico)
(vertico-mode t)

;; eldoc tweak
(add-hook
 'eglot-managed-mode-hook
 (lambda ()
   ;; we want eglot to setup callbacks from eldoc, but we don't want eldoc
   ;; running after every command. As a workaround, we disable it after we just
   ;; enabled it. Now calling `M-x eldoc` will put the help we want in the eldoc
   ;; buffer. Alternatively we could tell eglot to stay out of eldoc, and add the
   ;; hooks manually, but that seems fragile to updates in eglot.
   (eldoc-mode -1)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; buffer customization
(setq display-buffer-alist nil)
(setq display-buffer-alist
      '(
	;; occur-mode
	(
	 (derived-mode . occur-mode)
	 (display-buffer-reuse-mode-window
	  display-buffer-below-selected)
	 (window-height . 8)
	 (dedicated . t)
	 (body-function . (lambda(window) (select-window window))))
	;; compilation-mode
	(
	 (derived-mode . compilation-mode)
	 (display-buffer-reuse-mode-window
	  display-buffer-below-selected)
	 (window-height . fit-window-to-buffer)
	 (dedicated . t)
	 (body-function . (lambda(window) (select-window window))))
	;; help-mode (Output)
	(
	 (derived-mode . help-mode)
	 (display-buffer-reuse-mode-window)
	 (dedicated . t)
	 (body-function . (lambda(window) (select-window window))))
	;; diff-mode
	(
	 (derived-mode . diff-mode)
	 (display-buffer-reuse-mode-window)
	 (dedicated . t)
	 (body-function . (lambda(window) (select-window window))))
	)
      )
