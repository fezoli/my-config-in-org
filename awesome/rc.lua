-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
-- local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- SYSTRAY
local systray = wibox.widget.systray();
systray:set_base_size(18)
--systray:set_valign("center")

-- EXTRA PACKAGES --
local batt = require("battery-widget")
local battery = batt{ ac_prefix = { 
                                { 15, "<span color=\"red\"></span>"},
        			{ 25, "<span color=\"orange\"></span>"},
        			{ 50, "<span color=\"yellow\"></span>"},
        			{ 75, "<span color=\"green\"></span>"},
        			{100, ""},
    		      },
		      battery_prefix = {
		                { 15, "<span color=\"red\"></span>"},
        			{ 25, "<span color=\"red\"></span>"},
        			{ 50, "<span color=\"orange\"></span>"},
        			{ 75, "<span color=\"yellow\"></span>"},
        			{100, "<span color=\"green\"></span>"},
    		      },
                      widget_font = "FontAwesome 11" ,
                      widget_text = "${AC_BAT}",
                      tooltip_text = "Battery ${state}${time_est} (${percent}%)",
                      battery_full_icon = "" }
                      
local cyclefocus = require('cyclefocus')
cyclefocus.display_notifications = false

local cpu = awful.widget.watch("cpu.fish", 1)


-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
		     title = "Oops, there were errors during startup!",
		     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
	-- Make sure we don't go into an endless error loop
	if in_error then return end
	in_error = true

	naughty.notify({ preset = naughty.config.presets.critical,
			 title = "Oops, an error happened!",
			 text = tostring(err) })
	in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init("~/.config/awesome/theme.lua")

-- This is used later as the default terminal and editor to run.
terminal = "urxvt" or "xterm"
screenlock = "xscreensaver-command --lock"
screenshot = "screenshot.sh"
powermenu  = "xterm -e power.sh"
editor = os.getenv("EDITOR") or "jed" or "emacs -nw"
editor_cmd = terminal .. " -e " .. editor

-- CUSTOM INITIALIZATIONS --
-- battery.widget_font = beautiful.font
cpu:connect_signal("button::press", function() awful.spawn(terminal.." -e top") end)
cpu.font = beautiful.font
cpu.fg_normal = "#440000"

naughty.config.notify_callback = function(args)
   args.timeout = 0
   return args
end

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
   awful.layout.suit.tile,
   awful.layout.suit.floating,
   --    awful.layout.suit.tile.left,
   awful.layout.suit.tile.bottom,
   --    awful.layout.suit.tile.top,
   --    awful.layout.suit.fair,
   --    awful.layout.suit.fair.horizontal,
   --    awful.layout.suit.spiral,
   --    awful.layout.suit.spiral.dwindle,
   awful.layout.suit.max,
   --    awful.layout.suit.max.fullscreen,
   --    awful.layout.suit.magnifier,
   --    awful.layout.suit.corner.nw,
   -- awful.layout.suit.corner.ne,
   -- awful.layout.suit.corner.sw,
   -- awful.layout.suit.corner.se,
}


mytextclock = wibox.widget.textclock()
-- mytextclock.font = "JetBrains Mono 10 Bold"

--awful.widget.watch('sh /home/fekete/bin/print_status.sh', 15),


local function set_wallpaper(s)
   -- Wallpaper
   if beautiful.wallpaper then
      local wallpaper = beautiful.wallpaper
      -- If wallpaper is a function, call it with the screen
      if type(wallpaper) == "function" then
	 wallpaper = wallpaper(s)
      end
      gears.wallpaper.maximized(wallpaper, s, true)
   end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
      awful.tag.add("web", {
--		       icon               = "/path/to/icon1.png",
		       layout             = awful.layout.suit.float,
		       -- master_fill_policy = "master_width_factor",
		       --gap_single_client  = true,
		       --gap                = 15,
		       screen             = s,
		       selected           = true,
      })
      awful.tag.add("dev", {
--		       icon               = "/path/to/icon1.png",
		       layout             = awful.layout.suit.tile.bottom,
		       -- master_fill_policy = "master_width_factor",
		       --gap_single_client  = false,
		       --gap                = 15,
		       screen             = s,
		       selected           = false,
      })
      awful.tag.add("loc", {
--		       icon               = "/path/to/icon1.png",
		       layout             = awful.layout.suit.tile,
		       -- master_fill_policy = "master_width_factor",
		       --gap_single_client  = false,
		       --gap                = 15,
		       screen             = s,
		       selected           = false,
      })
      awful.tag.add("rem", {
--		       icon               = "/path/to/icon1.png",
		       layout             = awful.layout.suit.tile,
		       -- master_fill_policy = "master_width_factor",
		       --gap_single_client  = false,
		       --gap                = 15,
		       screen             = s,
		       selected           = false,
      })
      awful.tag.add("vrt", {
--		       icon               = "/path/to/icon1.png",
		       layout             = awful.layout.suit.float,
		       -- master_fill_policy = "master_width_factor",
		       --gap_single_client  = false,
		       --gap                = 15,
		       screen             = s,
		       selected           = false,
      })
      awful.tag.add("vnc", {
--		       icon               = "/path/to/icon1.png",
		       layout             = awful.layout.suit.float,
		       -- master_fill_policy = "master_width_factor",
		       --gap_single_client  = false,
		       --gap                = 15,
		       screen             = s,
		       selected           = false,
      })
      awful.tag.add("doc", {
--		       icon               = "/path/to/icon1.png",
		       layout             = awful.layout.suit.float,
		       -- master_fill_policy = "master_width_factor",
		       --gap_single_client  = false,
		       --gap                = 15,
		       screen             = s,
		       selected           = false,
      })
      awful.tag.add("cm1", {
--		       icon               = "/path/to/icon1.png",
		       layout             = awful.layout.suit.tile,
		       -- master_fill_policy = "master_width_factor",
		       --gap_single_client  = false,
		       --gap                = 15,
		       screen             = s,
		       selected           = false,
      })
      awful.tag.add("cm2", {
--		       icon               = "/path/to/icon1.png",
		       layout             = awful.layout.suit.tile,
		       -- master_fill_policy = "master_width_factor",
		       --gap_single_client  = false,
		       --gap                = 15,
		       screen             = s,
		       selected           = false,
      })

      local taglist_buttons = gears.table.join(
	 awful.button({ }, 1, function(t) t:view_only() end),
	 awful.button({ modkey }, 1, function(t)
	       if client.focus then
		  client.focus:move_to_tag(t)
	       end
	 end),
	 awful.button({ }, 3, awful.tag.viewtoggle),
	 awful.button({ modkey }, 3, function(t)
	       if client.focus then
		  client.focus:toggle_tag(t)
	       end
	 end),
	 awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
	 awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
      )

      local tasklist_buttons = gears.table.join(
	 awful.button({ }, 1, function (c)
	       if c == client.focus then
		  c.minimized = true
	       else
		  c:emit_signal(
		     "request::activate",
		     "tasklist",
		     {raise = true}
		  )
	       end
	 end),
	 awful.button({ }, 3, function()
	       awful.menu.client_list({ theme = { width = 250 } })
	 end),
	 awful.button({ }, 4, function ()
	       awful.client.focus.byidx(1)
	 end),
	 awful.button({ }, 5, function ()
	       awful.client.focus.byidx(-1)
      end))

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
			   awful.button({ }, 1, function () awful.layout.inc( 1) end),
			   awful.button({ }, 3, function () awful.layout.inc(-1) end),
			   awful.button({ }, 4, function () awful.layout.inc( 1) end),
			   awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
	screen  = s,
	filter  = awful.widget.taglist.filter.all,
	buttons = taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
	screen  = s,
	filter  = awful.widget.tasklist.filter.currenttags,
	buttons = tasklist_buttons,
	border_width = 0,
	style = {
	   bg_focus   = beautiful.bg_focus,
	   fg_focus   = beautiful.fg_tasklist,
	   font_focus = "monospace bold 10"
	}
    }

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
     s.mywibox:setup {
 	layout = wibox.layout.align.horizontal,
 	{ -- Left widgets
 	    layout = wibox.layout.fixed.horizontal,
 	    -- mylauncher,
 	    s.mytaglist,
	    wibox.widget.separator({orientation="horizontal", thickness=0, forced_width=10}),
 	    s.mypromptbox,
 	},
 	s.mytasklist, -- Middle widget
 	{ -- Right widgets
 	    layout = wibox.layout.fixed.horizontal,
 	    -- mykeyboardlayout,
	    --	    awful.widget.watch('sh /home/fekete/bin/print_status.sh', 15),
	    cpu,
	    battery,
	    wibox.widget.separator({orientation="horizontal", thickness=0, forced_width=10}),
	    systray,
	    wibox.widget.separator({orientation="horizontal", thickness=0, forced_width=10}),
	    mytextclock,
 	    s.mylayoutbox,
 	},
     }
end
) -- each screen setup


-- {{{ Mouse bindings
root.buttons(gears.table.join(
    --awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = gears.table.join(
    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
       {description="show help", group="awesome"}),

    awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
       {description = "view previous", group = "tag"}),

    awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
       {description = "view next", group = "tag"}),

    awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
       {description = "go back", group = "tag"}),

    awful.key({ modkey,           }, "j",
       function ()
	  awful.client.focus.byidx(1)
       end,
       {description = "focus next by index", group = "client"}
    ),

    awful.key({ modkey,           }, "k",
       function ()
	  awful.client.focus.byidx(-1)
       end,
       {description = "focus previous by index", group = "client"}
    ),

    --awful.key({ modkey,           }, "w", function () mymainmenu:show() end,
   --    {description = "show main menu", group = "awesome"}),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
	      {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
	      {description = "swap with previous client by index", group = "client"}),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
	      {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
	      {description = "focus the previous screen", group = "screen"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
	      {description = "jump to urgent client", group = "client"}),

    -- ALT-TAB --
    -- Alt-Tab: cycle through clients on the same screen.
    -- This must be a clientkeys mapping to have source_c available in the callback.
    cyclefocus.key({ "Mod1", }, "Tab", {
	  -- cycle_filters as a function callback:
	  --cycle_filters = { function (c, source_c) return c.screen == source_c.screen end },

	  -- cycle_filters from the default filters:
	  cycle_filters = { cyclefocus.filters.same_screen, cyclefocus.filters.common_tag },
	  keys = {'Tab', 'ISO_Left_Tab'}  -- default, could be left out
    }),
    --awful.key({ "Mod1",           }, "Tab",
    --function ()
--	awful.client.focus.history.previous()
--	if client.focus then
--	   client.focus:raise()
--	end
  --  end),

--    awful.key({ "Mod1", "Shift"   }, "Tab",
  --  function ()
--	awful.client.focus.byidx(-1)
--	if client.focus then
--	    client.focus:raise()
--	end
  --  end),	      

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal)              end,
	      {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey, "Control"   }, "l",     function () awful.spawn(screenlock)           end,
	      {description = "Activate screen locking", group = "launcher"}),
    awful.key({                   }, "Print",     function () awful.spawn(screenshot)         end,
	      {description = "Create a screenshot", group = "launcher"}),

    awful.key({ modkey, "Control" }, "r", awesome.restart,
	      {description = "reload awesome", group = "awesome"}),

    awful.key({ modkey,           }, "q",     function () awful.spawn.easy_async_with_shell(powermenu)   end,
	      {description = "power menu", group = "awesome"}),

    awful.key({ modkey, "Shift"   }, "q", awesome.quit,
	      {description = "quit awesome", group = "awesome"}),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
	      {description = "increase master width factor", group = "layout"}),

    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
	      {description = "decrease master width factor", group = "layout"}),

    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
	      {description = "increase the number of master clients", group = "layout"}),

    awful.key({ modkey, "Control"   }, "Return",
       function () awful.tag.incnmaster(-1, nil, true) end,
	      {description = "decrease the number of master clients", group = "layout"}),

    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
	      {description = "increase the number of columns", group = "layout"}),

    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
	      {description = "decrease the number of columns", group = "layout"}),

    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
	      {description = "select next", group = "layout"}),

    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
	      {description = "select previous", group = "layout"}),

    awful.key({ modkey, "Control" }, "n",
       function ()
	  local c = awful.client.restore()
	  -- Focus restored client
	  if c then
	     c:emit_signal(
		"request::activate", "key.unminimize", {raise = true}
	     )
	  end
       end,
       {description = "restore minimized", group = "client"}),

    -- dmenu
    awful.key({ modkey },            "r",     function ()
	  awful.util.spawn("dmenu_run -i -p 'Run program: ' -nb '" .. 
			   beautiful.bg_normal .. "' -nf '" .. beautiful.fg_normal .. 
			   "' -sb '" .. beautiful.bg_focus .. 
			   "' -sf '" .. beautiful.fg_focus .. "'") 
    end),

    awful.key({ modkey }, "x",
       function ()
	  awful.prompt.run {
	     prompt       = "Run Lua code: ",
	     textbox      = awful.screen.focused().mypromptbox.widget,
	     exe_callback = awful.util.eval,
	     history_path = awful.util.get_cache_dir() .. "/history_eval"
	  }
       end,
       {description = "lua execute prompt", group = "awesome"}),

    awful.key({ modkey, }, "F5", function ()
	  os.execute("/opt/bin/ts_brightness.sh -") end,
       {description = "lighter", group = "ts"}),
    awful.key({ modkey }, "F6", function ()
	  os.execute("/opt/bin/ts_brightness.sh +") end,
       {description = "darker", group = "ts"}),

    -- Start programs --
    awful.key({ modkey, "Shift" }, "f", function() awful.spawn("firefox") end,
       {description = "Firefox", group = "launcher"}),
    awful.key({ modkey, "Shift" }, "p", function() awful.spawn("pavucontrol") end,
       {description = "Pavucontrol", group = "launcher"})
)

clientkeys = gears.table.join(
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end,
	      {description = "close", group = "client"}),
    awful.key({ modkey,           }, "f",      awful.client.floating.toggle                     ,
	      {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Shift" }, "Return", function (c) c:swap(awful.client.getmaster())   end,
	      {description = "move to master", group = "client"}),
    awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
	      {description = "move to screen", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
	      {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey, "Shift"   }, "s",      function (c) c.sticky = not c.sticky          end,
	      {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey,           }, "n",
	function (c)
	    -- The client currently has the input focus, so it cannot be
	    -- minimized, since minimized clients can't have the focus.
	    c.minimized = true
	end ,
	{description = "minimize", group = "client"}),
    awful.key({ modkey,           }, "m",
	function (c)
	    c.maximized = not c.maximized
	    c:raise()
	end ,
	{description = "(un)maximize", group = "client"}),
    awful.key({ modkey, "Control" }, "m",
	function (c)
	    c.maximized_vertical = not c.maximized_vertical
	    c:raise()
	end ,
	{description = "(un)maximize vertically", group = "client"}),
    awful.key({ modkey, "Shift"   }, "m",
	function (c)
	    c.maximized_horizontal = not c.maximized_horizontal
	    c:raise()
	end ,
	{description = "(un)maximize horizontally", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
	-- View tag only.
	awful.key({ modkey }, "#" .. i + 9,
		  function ()
			local screen = awful.screen.focused()
			local tag = screen.tags[i]
			if tag then
			   tag:view_only()
			end
		  end,
		  {description = "view tag #"..i, group = "tag"}),
	-- Toggle tag display.
	awful.key({ modkey, "Control" }, "#" .. i + 9,
		  function ()
		      local screen = awful.screen.focused()
		      local tag = screen.tags[i]
		      if tag then
			 awful.tag.viewtoggle(tag)
		      end
		  end,
		  {description = "toggle tag #" .. i, group = "tag"}),
	-- Move client to tag.
	awful.key({ modkey, "Shift" }, "#" .. i + 9,
		  function ()
		      if client.focus then
			  local tag = client.focus.screen.tags[i]
			  if tag then
			      client.focus:move_to_tag(tag)
			  end
		     end
		  end,
		  {description = "move focused client to tag #"..i, group = "tag"}),
	-- Toggle tag on focused client.
	awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
		  function ()
		      if client.focus then
			  local tag = client.focus.screen.tags[i]
			  if tag then
			      client.focus:toggle_tag(tag)
			  end
		      end
		  end,
		  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c)
	c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ modkey }, 1, function (c)
	c:emit_signal("request::activate", "mouse_click", {raise = true})
	awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
	c:emit_signal("request::activate", "mouse_click", {raise = true})
	awful.mouse.client.resize(c)
    end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
		     border_color = beautiful.border_normal,
		     focus = awful.client.focus.filter,
		     raise = true,
		     keys = clientkeys,
		     buttons = clientbuttons,
		     screen = awful.screen.preferred,
		     placement = awful.placement.no_overlap+awful.placement.no_offscreen,
		     callback = awful.client.setslave		     
     }
    },

    -- Floating clients.
    { rule_any = {
	instance = {
	  "DTA",  -- Firefox addon DownThemAll.
	  "copyq",  -- Includes session name in class.
	  "pinentry",
	},
	class = {
	  "Arandr",
	  "Blueman-manager",
	  "Gpick",
	  "Kruler",
	  "Message",  -- kalarm.
	  "Sxiv",
	  "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
	  "Wpa_gui",
	  "textdialogslot",
	  "xtightvncviewer",
	  "telexHistoryReport",
	  "OpsChangesWindow",
	  "ScheduleChangesWindow",
	  "scenariohandlerwindow",
	  "cnloverview",
	  "cnlupdate",
	  "Apache NetBeans IDE 19"},

	-- Note that the name property shown in xprop might be set slightly after creation of the client
	-- and the name shown there might not match defined rules here.
	name = {
	  "Event Tester",  -- xev.
	},
	role = {
	  "AlarmWindow",  -- Thunderbird's calendar.
	  "ConfigManager",  -- Thunderbird's about:config.
	  "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
	}
      }, properties = { floating = true }},

    -- Add titlebars to normal clients and dialogs
    --{ rule_any = {type = { "normal", "dialog" }
    { rule_any = { type = { "dialog" }
      }, properties = { titlebars_enabled = true }
    },

    { rule = { instance = "Navigator", class = "firefox", },
      properties = { maximized = true, border_width = 0 } },
    { rule = { instance = "firefox", class = "firefox", },
      properties = { maximized = false, border_width = 0 } },
    { rule = { instance = "code", class = "Code", },
      properties = { maximized = true, border_width = 0 } },
    { rule = { class = "Thunderbird" },
      properties = { screen = 1, tag = "web", maximized = true } },
    { rule = { class = "Gitk" },
      properties = { maximized = true } },
    { rule = { class = "MainPanel" },
      properties = { maximized = true } },
    { rule = { class = "Emacs" },
      properties = { screen = 1, tag = "dev", size_hints_honor = false } },
    { rule_any = { class = {"NetLine/Slot", 
			    "NetLine/Sched",
			    "comparewindow",
			    "telexhandlerwindow",
			    "linkslotswindow",
			    "Sxmnmain"} },
      properties = { tag = "dev",
		     floating = true,
		     placement = awful.placement.centered } },
		     -- width = awful.screen.focused().workarea.width * 0.75,
		     --height = awful.screen.focused().workarea.height * 0.75 } },
    { rule = { class = "Soffice" },
      properties = { tag = "doc" } },
    { rule = { class = "URxvt" },
      properties = {
	placement = awful.placement.centered,
	width = awful.screen.focused().workarea.width * 0.5,
	height = awful.screen.focused().workarea.height * 0.5,
	size_hints_honor = false
      }
    },
    { rule = { class = "Pavucontrol" },
      properties = {
	floating = true,
	placement = awful.placement.centered,
	width = awful.screen.focused().workarea.width * 0.5,
	height = awful.screen.focused().workarea.height * 0.5,
	y = awful.screen.focused().workarea.height * 0.25}},
}

-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
	-- Prevent clients from being unreachable after screen count changes.
	awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
	awful.button({ }, 1, function()
	    c:emit_signal("request::activate", "titlebar", {raise = true})
	    awful.mouse.client.move(c)
	end),
	awful.button({ }, 3, function()
	    c:emit_signal("request::activate", "titlebar", {raise = true})
	    awful.mouse.client.resize(c)
	end)
    )

    awful.titlebar(c) : setup {
	{ -- Left
	    awful.titlebar.widget.iconwidget(c),
	    buttons = buttons,
	    layout  = wibox.layout.fixed.horizontal
	},
	{ -- Middle
	    { -- Title
		align  = "center",
		widget = awful.titlebar.widget.titlewidget(c)
	    },
	    buttons = buttons,
	    layout  = wibox.layout.flex.horizontal
	},
	{ -- Right
	    awful.titlebar.widget.floatingbutton (c),
	    awful.titlebar.widget.maximizedbutton(c),
	    awful.titlebar.widget.stickybutton   (c),
	    awful.titlebar.widget.ontopbutton    (c),
	    awful.titlebar.widget.closebutton    (c),
	    layout = wibox.layout.fixed.horizontal()
	},
	layout = wibox.layout.align.horizontal
    }
end
)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", {raise = false})
end
)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

-- awful.util.spawn_with_shell("~/bin/set_monitor.fish")
-- awful.util.spawn_with_shell("gnome-keyring-daemon --replace")
awful.util.spawn_with_shell("nm-applet")
-- awful.spawn.single_instance("nextcloud")
awful.spawn.single_instance("keychain")
awful.spawn.easy_async("xscreensaver")
awful.spawn.easy_async("setxkbmap -option ctrl:nocaps hu")
awful.spawn.easy_async("numlockx on")
awful.spawn.easy_async("xset b off")

-- awful.spawn.easy_async("xinput set-prop \"Elan Touchpad\" \"libinput Natural Scrolling Enabled\" 1")
-- awful.spawn.easy_async("xinput set-prop \"Elan Touchpad\" \"libinput Tapping Enabled\" 1")
-- awful.spawn.easy_async("xinput disable \"RAYD0001:00 2386:3118\"")
